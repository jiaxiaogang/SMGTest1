//
//  WordConcreteFactory.h
//  XGSmartHW
//
//  Created by 贾  on 2017/3/10.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  MARK:--------------------具象词,生成工厂--------------------
 *  使用装饰者模式和建造者模式;控制整个生成词汇的过程;
 *
 *  保证:精准的提问
 *      1级:你叫什么,你是什么,你能干嘛;等...
 *  保证:词汇的数据保留及有效
 *
 */
@class WordConcrete;
@protocol WordConcreteFactoryDelegate <NSObject>

-(void) wordConcreteFactory_Finish:(WordConcrete*)wordConcrete;
-(void) wordConcreteFactory_Failed:(NSString*)errorStr;
-(void) wordConcreteFactory_NextQuestion:(void(^)(NSString *answerStr))answerComplete withQuestion:(NSString*)question;

@end

@interface WordConcreteFactory : NSObject

@property (weak, nonatomic) id<WordConcreteFactoryDelegate> delegate;

-(void) startWord;                           //新建一条词汇;

@end
