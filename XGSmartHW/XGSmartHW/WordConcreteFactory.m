//
//  WordConcreteFactory.m
//  XGSmartHW
//
//  Created by 贾  on 2017/3/10.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "WordConcreteFactory.h"
#import "WordConcrete.h"
#import "SMG_DAO.h"

@interface WordConcreteFactory ()

@property (strong,nonatomic) WordConcrete *wordConcrete;

@end

@implementation WordConcreteFactory


-(void) startWord{
    if (self.wordConcrete != nil && self.delegate && [self.delegate respondsToSelector:@selector(wordConcreteFactory_Failed:)]) {
        [self.delegate wordConcreteFactory_Failed:@"新已来,旧未去.二选一.我决定喜新厌旧!"];
    }
    self.wordConcrete = [[WordConcrete alloc] init];
    self.wordConcrete.wordId = [SMG_Dao createWordId];
    [self createQuestionAndDelegate];
}

-(void) createQuestionAndDelegate{
    if (self.wordConcrete) {
        if(self.wordConcrete.text == nil){
            if (self.delegate && [self.delegate respondsToSelector:@selector(wordConcreteFactory_NextQuestion:withQuestion:)]) {
                __weak __typeof(self)weakSelf = self;
                [self.delegate wordConcreteFactory_NextQuestion:^(NSString *answerStr) {
                    weakSelf.wordConcrete.text = NSSTRINGTOISVALID(answerStr);
                    [weakSelf createQuestionAndDelegate];
                } withQuestion:@"你要调教我什么词汇"];
            }
        }else if([self.wordConcrete.attributes objectForKey:NSWordConcreteAttributeName_Hardness] == nil){
            if (self.delegate && [self.delegate respondsToSelector:@selector(wordConcreteFactory_NextQuestion:withQuestion:)]) {
                __weak __typeof(self)weakSelf = self;
                [self.delegate wordConcreteFactory_NextQuestion:^(NSString *answerStr) {
                    [weakSelf.wordConcrete.attributes setObject:NSSTRINGTOISVALID(answerStr) forKey:NSWordConcreteAttributeName_Hardness];
                    [weakSelf createQuestionAndDelegate];
                } withQuestion:@"这玩意儿多硬?(里氏硬度)"];
            }
        }else{
            if (self.delegate && [self.delegate respondsToSelector:@selector(wordConcreteFactory_Finish:)]) {
                [self.delegate wordConcreteFactory_Finish:self.wordConcrete];
            }
        }
    }else{
        if (self.delegate && [self.delegate respondsToSelector:@selector(wordConcreteFactory_Failed:)]) {
            [self.delegate wordConcreteFactory_Failed:@"生成失败!!!我会拼命查出bug,并打电话通知你."];
        }
    }
}



@end
