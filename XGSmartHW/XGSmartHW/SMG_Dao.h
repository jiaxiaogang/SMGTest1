//
//  SMG_Dao.h
//  XGSmartHW
//
//  Created by 贾  on 2017/3/10.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import <Foundation/Foundation.h>


/**
 *  MARK:--------------------SMG的大脑存储类--------------------
 *  使用KV;
 *  KEY     :   每个词汇编id
 *  VALUE   :   每个词汇对应一组释义(释义需要数字化,比如描述为食物,要跟着描述味道,热量,物理密度,物理形态,产生来源等)
 
 
 *
 */
@class WordConcrete;
@interface SMG_Dao : NSObject

@property (strong,nonatomic) NSMutableDictionary *normalDao;    //普通池
@property (strong,nonatomic) NSMutableDictionary *cacheDao;     //遗忘池(超过10万条或3个月时放到遗忘池)(这个策略可以根据机能来自动变化)
-(id) init;
+(long long)createWordId;
-(void) updateWord:(WordConcrete*)wordC;         //更新词汇
-(WordConcrete*) getWordConcreteWithWordText:(NSString*)wordText;
@end





