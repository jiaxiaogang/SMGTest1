//
//  WordBase.m
//  XGSmartHW
//
//  Created by 贾  on 2017/3/10.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "WordBase.h"


@implementation WordBase

- (void)encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:NSSTRINGTOISVALID(self.text) forKey:@"text"];
    [encoder encodeInt64:self.wordId forKey:@"wordId"];
}

- (id)initWithCoder:(NSCoder *)decoder{
    self = [super init];
    if (self){
        self.text = [decoder decodeObjectForKey:@"text"];
        self.wordId = [decoder decodeInt64ForKey:@"wordId"];
    }
    return self;
}

@end
