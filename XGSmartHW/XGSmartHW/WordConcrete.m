//
//  WordConcrete.m
//  XGSmartHW
//
//  Created by 贾  on 2017/3/10.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "WordConcrete.h"

@implementation WordConcrete

-(NSMutableDictionary*) attributes{
    if (_attributes == nil) {
        _attributes = [[NSMutableDictionary alloc]init];
    }
    return _attributes;
}

- (void)encodeWithCoder:(NSCoder *)encoder{
    [super encodeWithCoder:encoder];
    [encoder encodeObject:self.attributes forKey:@"attributes"];
}

- (id)initWithCoder:(NSCoder *)decoder{
    self = [super initWithCoder:decoder];
    if (self){
        self.attributes = [decoder decodeObjectForKey:@"attributes"];
    }
    return self;
}


@end
