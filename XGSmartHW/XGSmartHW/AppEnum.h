//
//  AppEnum.h
//  XGSmartHW
//
//  Created by 贾  on 2017/3/10.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

/**
 *  MARK:--------------------文章类别--------------------
 */
typedef NS_ENUM(NSInteger, FoodType) {
    FoodType_None           = 0,    //非食物
    FoodType_Vegetables     = 1,    //蔬菜
    FoodType_Fruits         = 2,    //水果
    FoodType_Meat           = 3,    //肉
};
