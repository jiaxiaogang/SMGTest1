//
//  TempleteShadowWindow.m
//  XG_Window_IOS
//
//  Created by 贾  on 16/5/27.
//  Copyright © 2016年 XG_Window_IOS. All rights reserved.
//  https://www.github.com/jiaxiaogang/XG_Window_IOS
//

#import "VEWindowManager.h"
#import "VEWindowBase.h"

@implementation VEWindowManager

+(void) openWindow:(VEWindowBase*)window withParentView:(UIView*)parentView animation:(BOOL)animation{
    if (window && parentView) {
        if ([window superview]) {
            [window removeFromSuperview];
        }
        [parentView addSubview:window];
        window.animationSwitch = animation;
        [window Open];
    }
}

@end
