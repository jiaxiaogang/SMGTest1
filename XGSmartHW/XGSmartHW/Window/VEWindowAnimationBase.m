//
//  TempleteShadowWindow.m
//  XG_Window_IOS
//
//  Created by 贾  on 16/5/27.
//  Copyright © 2016年 XG_Window_IOS. All rights reserved.
//  https://www.github.com/jiaxiaogang/XG_Window_IOS
//

#import "VEWindowAnimationBase.h"


@implementation VEWindowAnimationBase

-(void) animation_Open:(UIView *)animationObj completion:(void (^)(BOOL))completion{
    if (self.duration_open < 0) {
        self.duration_open = 0;
    }
}
-(void) animation_Close:(UIView *)animationObj completion:(void (^)(BOOL))completion{
    if (self.duration_close < 0) {
        self.duration_close = 0;
    }
}

@end
