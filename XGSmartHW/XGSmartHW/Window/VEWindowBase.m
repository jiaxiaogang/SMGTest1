//
//  TempleteShadowWindow.m
//  XG_Window_IOS
//
//  Created by 贾  on 16/5/27.
//  Copyright © 2016年 XG_Window_IOS. All rights reserved.
//  https://www.github.com/jiaxiaogang/XG_Window_IOS
//

#import "VEWindowBase.h"
#import "VEWindowAnimationNormal.h"

@implementation VEWindowBase

/**
 *  MARK:--------------------init--------------------
 *  无论哪个方法初始化,都会进入到init方法;
 */
-(id) init{
    self = [super init];
    if (self) {
    }
    return self;
}

-(void) initView{
    
}
-(void) initData{
    self.animationSwitch = true;
}
-(void) initDisplay{
    
}

/**
 *  MARK:--------------------IVEWindow--------------------
 */
-(void) Open{
    [self willAppear:self.animationSwitch];
}

-(void) willAppear:(BOOL)animation{
    if (animation) {
        [self.animationObj animation_Open:[self animation_OpenObj] completion:^(BOOL finished) {
            [self didAppear];
        }];
    }else{
        [self didAppear];
    }
}

-(void) didAppear{
    if (self.delegate && [self.delegate respondsToSelector:@selector(veWindow_DidAppear:)]) {
        [self.delegate veWindow_DidAppear:self];
    }
}

-(void) Close{
    [self willDisappear:self.animationSwitch];
}
-(void) willDisappear:(BOOL)animation{
    if (animation) {
        [self.animationObj animation_Close:[self animation_CloseObj] completion:^(BOOL finished) {
            [self didDisappear];
        }];
    }else{
        [self didDisappear];
    }
}
-(void) didDisappear{
    if (self.delegate && [self.delegate respondsToSelector:@selector(veWindow_DidDisappear:)]) {
        [self.delegate veWindow_DidDisappear:self];
    }
    if ([self superview]) {
        [self removeFromSuperview];
    }
}
//获取打开动画部分的view;默认为全部窗口
-(UIView*) animation_OpenObj{
    return self;
}
//获取关闭动画部分的view;默认为全部窗口
-(UIView*) animation_CloseObj{
    return self;
}

/**
 *  MARK:--------------------method--------------------
 */
-(VEWindowAnimationBase*) animationObj{
    if (!_animationObj) {
        _animationObj = [[VEWindowAnimationNormal alloc] init]; //默认动画
    }
    return _animationObj;
}

@end
