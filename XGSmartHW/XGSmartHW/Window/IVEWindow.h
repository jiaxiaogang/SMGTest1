//
//  TempleteShadowWindow.m
//  XG_Window_IOS
//
//  Created by 贾  on 16/5/27.
//  Copyright © 2016年 XG_Window_IOS. All rights reserved.
//  https://www.github.com/jiaxiaogang/XG_Window_IOS
//

#import <Foundation/Foundation.h>

@protocol IVEWindow <NSObject>

-(void) initView;
-(void) initData;
-(void) initDisplay;
-(void) Open;
-(void) willAppear:(BOOL)animation;
-(void) didAppear;
-(void) Close;
-(void) willDisappear:(BOOL)animation;
-(void) didDisappear;

@end
