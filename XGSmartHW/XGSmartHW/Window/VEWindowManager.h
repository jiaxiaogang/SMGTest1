//
//  TempleteShadowWindow.m
//  XG_Window_IOS
//
//  Created by 贾  on 16/5/27.
//  Copyright © 2016年 XG_Window_IOS. All rights reserved.
//  https://www.github.com/jiaxiaogang/XG_Window_IOS
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class VEWindowBase;
@interface VEWindowManager : NSObject

+(void) openWindow:(VEWindowBase*)window withParentView:(UIView*)parentView animation:(BOOL)animation;
                    
@end
