//
//  SMG.m
//  XGSmartHW
//
//  Created by 贾  on 2017/3/10.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "SMG.h"
#import "WordConcrete.h"
#import "SMG_Dao.h"
#import "TransferTeacher.h"

static SMG *instance = nil;

@interface SMG ()

@property (strong,nonatomic) SMG_Dao *dao;

@end

@implementation SMG


+(SMG*) shareInstance{
    if (instance == nil) {
        @synchronized(self){
            instance = [[SMG alloc] init];
            instance.dao = [[SMG_Dao alloc] init];
        }
    }
    return instance;
}

-(BOOL) checkValidWithWordConcrete:(WordConcrete*)wordC{
    if (wordC && wordC.wordId >= 0 && NSSTRINGISVALID(wordC.text)) {
        return true;
    }
    return false;
}

-(void) updateWordConcrete:(WordConcrete*)wordC withTransferTeacher:(TransferTeacher*)tt{
    if (wordC && tt) {
        WordConcrete *oldWordC = [self.dao getWordConcreteWithWordText:wordC.text];//本地旧数据
        //4,判断修正_(可信度,wordId)
        [wordC.attributes setObject:NSSTRINGTOISVALID(tt.name) forKey:NSWordConcreteAttributeName_TtansferTeacher];//xxx临时这么写
        [wordC.attributes setObject:@(0.5f) forKey:NSWordConcreteAttributeName_TrustValue]; //xxx临时这么写;
        //5,SMG_Dao收录词汇;
        [self.dao updateWord:wordC];
    }
}

-(WordConcrete*) getWordConcreteWithWordText:(NSString*)wordText{
    return [self.dao getWordConcreteWithWordText:wordText];
}

-(NSString*) getHardness:(NSString*)wordText{
    WordConcrete *wordC = [self getWordConcreteWithWordText:wordText];
    if (wordC) {
        if ([wordC.attributes objectForKey:NSWordConcreteAttributeName_Hardness]) {
            return [NSString stringWithFormat:@"查到了,硬度:%@",[wordC.attributes objectForKey:NSWordConcreteAttributeName_Hardness]];
        }else{
            return @"认识词,但它硬度我不知";
        }
    }else{
        return @"不认识这词";
    }
}

@end


