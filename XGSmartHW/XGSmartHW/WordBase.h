//
//  WordBase.h
//  XGSmartHW
//
//  Created by 贾  on 2017/3/10.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WordProperty_T.h"
#import "WordProperty_F.h"

/**
 *  MARK:--------------------词汇基类--------------------
 *  SMG的大脑存储基于词汇字符串;(词汇是SMG_Dao的大脑的因子)
 *  将抽象词汇的子类树形结构化;
 *  将具象词汇设为attributes来学习;
 *
 *  _t后辍:确定属性,即代码写死;不可更改;
 *  _f后辍:不确定属性,即自主学习的额外属性,活动属性;
 *
 */

@interface WordBase : NSObject <NSCoding>

//@property (strong,nonatomic) WordProperty_T *property_T;    //确定属性(例如苹果:食物)
//@property (strong,nonatomic) WordProperty_F *property_F;    //不确定属性

@property (strong,nonatomic) NSString *text;
@property (assign, nonatomic) long long wordId;                 //词汇ID

@end
