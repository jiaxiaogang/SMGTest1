//
//  TransferTeacher.h
//  XGSmartHW
//
//  Created by 贾  on 2017/3/10.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *  MARK:--------------------调教师类--------------------
 */
@class WordConcrete;
@interface TransferTeacher : NSObject

@property (strong,nonatomic,readonly) NSString *name;

-(id) initWithName:(NSString*)name;
-(void) trainingSMG:(WordConcrete*)wordC;

@end
