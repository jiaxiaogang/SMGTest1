//
//  ViewController.m
//  XGSmartHW
//
//  Created by 贾  on 2017/3/10.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "ViewController.h"
#import "TransferTeacher.h"
#import "WordConcreteFactory.h"
#import "TempleteShadowWindow.h"
#import "VEWindowManager.h"
#import "SMG.h"
#import "WordConcrete.h"

@interface ViewController ()<UITextFieldDelegate,WordConcreteFactoryDelegate>

@property (weak, nonatomic) IBOutlet UITextField *questionTF;
@property (weak, nonatomic) IBOutlet UITextField *answerTF;
@property (strong,nonatomic) WordConcreteFactory *wordConcreteFactory;
@property (strong,nonatomic) NSString *curTransferTeacherName;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initView];
    [self initData];
    [self initDisplay];
}

-(void) initView{
    [self.answerTF setUserInteractionEnabled:false];
}

-(void) initData{
    self.wordConcreteFactory = [[WordConcreteFactory alloc] init];
}

-(void) initDisplay{
    self.wordConcreteFactory.delegate = self;
    self.questionTF.delegate = self;
}
/**
 *  MARK:--------------------onclick--------------------
 */
- (IBAction)smallChiOnClick:(id)sender {
    [self startTraining:@"smallChi"];
}

- (IBAction)smallBiOnClick:(id)sender {
    [self startTraining:@"smallBi"];
}

- (IBAction)smallShengOnClick:(id)sender {
    [self startTraining:@"smallSheng"];
}

//1,让WordConcreteFactory收集词汇信息
-(void) startTraining:(NSString*)name{
    self.curTransferTeacherName = name;
    [self.wordConcreteFactory startWord];
}

//2,把生成的wordConcrete交由调教师来告诉SMG
-(void) commitWordToSMG:(WordConcrete*)wordC{
    if (NSSTRINGISVALID(self.curTransferTeacherName)) {
        //3,SMG重新判断 词汇有效性 和 可信度等信息,
        //xxx
        //4,SMG_Dao收录词汇;
        TransferTeacher *tt = [[TransferTeacher alloc] initWithName:self.curTransferTeacherName];
        [tt trainingSMG:wordC];
    }
}

/**
 *  MARK:--------------------UITextFieldDelegate--------------------
 */
- (void)textFieldDidBeginEditing:(UITextField *)textField{
    [self.answerTF setText:@""];
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    NSString *smgAnswer = [[SMG shareInstance] getHardness:textField.text];
    [self.answerTF setText:NSSTRINGTOISVALID(smgAnswer)];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.questionTF resignFirstResponder];
    return false;
}


/**
 *  MARK:--------------------WordConcreteFactoryDelegate--------------------
 */
-(void) wordConcreteFactory_Finish:(WordConcrete*)wordConcrete{
    [self commitWordToSMG:wordConcrete];//回答完毕,知识入脑
}
-(void) wordConcreteFactory_Failed:(NSString*)errorStr{
    NSLog(@"教学失败,哪个调教师教的?这都教不会?");
}
-(void) wordConcreteFactory_NextQuestion:(void(^)(NSString *answerStr))answerComplete withQuestion:(NSString*)question{
    TempleteShadowWindow *window = [[TempleteShadowWindow alloc] init];
    [window setFrame:CGRectMake(0, 0, ScreenWidth, ScreenHeight)];
    [window setData:question withConfirmBlock:answerComplete];
    [VEWindowManager openWindow:window withParentView:self.view animation:true];    
}





@end
