//
//  TransferTeacher.m
//  XGSmartHW
//
//  Created by 贾  on 2017/3/10.
//  Copyright © 2017年 XiaoGang. All rights reserved.
//

#import "TransferTeacher.h"
#import "WordConcrete.h"
#import "SMG.h"

@implementation TransferTeacher


-(id) initWithName:(NSString*)name{
    self = [super init];
    if (self) {
        _name = NSSTRINGTOISVALID(name);
    }
    return self;
}

-(void) trainingSMG:(WordConcrete*)wordC{
    //3,SMG重新判断 词汇有效性
    if (wordC && [[SMG shareInstance] checkValidWithWordConcrete:wordC]) {
        [[SMG shareInstance] updateWordConcrete:wordC withTransferTeacher:self];
    }
}

@end
